const { getImageUrl } = require("../utils/cloudinary");
const adminModel = require("../model/db");

const addCloth = async (req, res) => {
  try {
    const file = req.file;
    const imageUrl = await getImageUrl(file.path);

    // Extract data from the request body
    const { clothe_name, size, desc, price } = req.body;

    // Validate required fields
    if (!clothe_name) {
      return res.status(400).json({ error: "Clothe name is required." });
    }

    // Validate enum values for size
    const validSizes = ["small", "medium", "large"];
    if (!validSizes.includes(size)) {
      return res.status(400).json({ error: "Invalid size value." });
    }

    // Create a new admin document with the provided data
    const newAdmin = new adminModel({
      image: {
        clothes: imageUrl, // Set the image URL obtained from Cloudinary
        size,
        clothe_name,
        desc,
        price,
      },
    });

    // Save the new admin document to the database
    await newAdmin.save();

    // Respond with success message
    res.status(201).json({ data: newAdmin, status: "success" });
  } catch (err) {
    // Handle any errors that occur during the process
    res.status(500).json({ error: err.message });
  }
};

const updateCloth = async (req, res) => {
  try {
    const { id } = req.params;
    const file = req.file;
    const imageUrl = await getImageUrl(file.path);

    // Extract data from the request body
    const { clothe_name, size, desc, price } = req.body;

    // Validate enum values for size
    const validSizes = ["small", "medium", "large"];
    if (!validSizes.includes(size)) {
      return res.status(400).json({ error: "Invalid size value." });
    }

    // Find and update the cloth by id
    const updatedCloth = await adminModel.findByIdAndUpdate(
      id,
      {
        image: {
          clothes: imageUrl, // Set the new image URL obtained from Cloudinary
          size,
          clothe_name,
          desc,
          price,
        },
      },
      { new: true }
    );

    // Check if the cloth exists
    if (!updatedCloth) {
      return res.status(404).json({ error: "Cloth not found" });
    }

    // Respond with success message
    res.status(200).json({ data: updatedCloth, status: "success" });
  } catch (err) {
    // Handle any errors that occur during the process
    res.status(500).json({ error: err.message });
  }
};

// Delete Cloth
const deleteCloth = async (req, res) => {
  try {
    const { id } = req.params;

    // Find and delete the cloth by id
    const deletedCloth = await adminModel.findByIdAndDelete(id);

    // Check if the cloth exists
    if (!deletedCloth) {
      return res.status(404).json({ error: "Cloth not found" });
    }

    // Respond with success message
    res.status(200).json({ data: deletedCloth, status: "success" });
  } catch (err) {
    // Handle any errors that occur during the process
    res.status(500).json({ error: err.message });
  }
};

// Get All Cloths
const getAllCloths = async (req, res) => {
  try {
    // Retrieve all cloths from the database
    const allCloths = await adminModel.find();

    // Respond with the list of cloths
    res.status(200).json({ data: allCloths, status: "success" });
  } catch (err) {
    // Handle any errors that occur during the process
    res.status(500).json({ error: err.message });
  }
};

module.exports = {
  addCloth,
  updateCloth,
  deleteCloth,
  getAllCloths,
};
