const express = require("express");
const cors = require("cors");
const adminRoute = require("./routes/adminRoute");
require("dotenv").config();

const port = process.env.PORT || 5000;
const app = express();

app.use(cors());
// app.use(bodyParser.json())
app.use("/admin", adminRoute);
require("./config/dbConn");

app.listen(port, () => {
  console.log(`Server listening on port ${port}`);
});
