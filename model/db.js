// const mongoose = require('mongoose');
// const {Schema} = mongoose;

// const adminSchema = new Schema({
//     name:{type: String, require: true},
//     password:{type:String, require:true},
//     clothe_name: {type: String, require:true},
//     image: {
//         clothes: { type: String, default: "https://th.bing.com/th/id/R.19fa7497013a87bd77f7adb96beaf768?rik=144XvMigWWj2bw&riu=http%3a%2f%2fwww.pngall.com%2fwp-content%2fuploads%2f5%2fUser-Profile-PNG-High-Quality-Image.png&ehk=%2bat%2brmqQuJrWL609bAlrUPYgzj%2b%2f7L1ErXRTN6ZyxR0%3d&risl=&pid=ImgRaw&r=0" },
//         size: { type: String, enum: ['small', 'medium', 'large'], default: 'medium' }
//     }
// })

const mongoose = require("mongoose");

// Define your schema
const adminSchema = new mongoose.Schema({
  //   name: { type: String, required: true },
  //   password: { type: String, required: true },
  image: {
    clothes: { type: String, required: true },
    size: {
      type: String,
      enum: ["small", "medium", "large"],
      default: "medium",
    },
    clothe_name: { type: String, required: true },
    desc: { type: String },
    price: { type: Number },
  },
});

// Create and export the model
const AdminModel = mongoose.model("Admin", adminSchema);

module.exports = AdminModel;
