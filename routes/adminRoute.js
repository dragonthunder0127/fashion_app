const express = require("express");
const adminController = require("../controller/adminController");
const multer = require("../utils/multer");

const router = express.Router();

router.post("/upload", multer.single("image"), adminController.addCloth);
router.patch(
  "/updateUpload/:id",
  multer.single("image"),
  adminController.updateCloth
);
router.delete(
  "/deleteUpload/:id", adminController.deleteCloth
);
router.get("/getallUpload", adminController.getAllCloths);
module.exports = router;
